<?php
namespace Task;

use Exception;
use Mage\Task\AbstractTask;
use Mage\Task\ErrorWithMessageException;
use Mage\Task\SkipException;

class GrantPermissions extends AbstractTask
{

	/**
	 * Returns the Title of the Task
	 * @return string
	 */
	public function getName()
	{
		return "Grant permissions to Postgres tables";
	}

	/**
	 * Runs the task
	 *
	 * @return boolean
	 * @throws Exception
	 * @throws ErrorWithMessageException
	 * @throws SkipException
	 */
	public function run()
	{
		return $this->runCommandRemote("psql -d cp_devel01 -U c_install_cp < config/sql_privileges.sql");
	}
}
