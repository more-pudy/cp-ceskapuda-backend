<?php
use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\StyleBuilder;
use Box\Spout\Writer\WriterFactory;
use Silex\Application;
use Silex\Provider\DoctrineServiceProvider;
use Silex\Provider\MonologServiceProvider;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

define('PROJECT_ROOT', __DIR__ . '/..');

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../config/parameters.php';

$developmentMode = isset($_SERVER["APP_ENV"]) && $_SERVER["APP_ENV"] == "dev" ? true : false;

$app = new Silex\Application();

// enable debug mode
$app['debug'] = $developmentMode;

$app->register(new MonologServiceProvider(), array(
	'monolog.logfile' => PROJECT_ROOT . '/var/logs/app.log',
));

$app->register(new DoctrineServiceProvider(), array(
	'db.options' => array(
		'driver'	=> $driver,
		'host'		=> $host,
		'dbname'	=> $dbname,
		'user'		=> $user,
		'password'	=> $password
	),
));

$app->before(function (Request $request) {
	if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
		$data = json_decode($request->getContent(), true);
		$request->request->replace(is_array($data) ? $data : []);
	}
});

if ($developmentMode) {
	$app->after(function (Request $request, Response $response) {
		$response->headers->add([
			"Access-Control-Allow-Origin" => "http://localhost:4200",
			"Access-Control-Allow-Credentials" => "true",
			"Access-Control-Allow-Headers" => "Authorization,Content-Type,Accept,Origin,User-Agent,DNT,Cache-Control,X-Mx-ReqToken,Keep-Alive,X-Requested-With,If-Modified-Since",
			"Access-Control-Allow-Methods" => "GET, POST, OPTIONS, PUT, DELETE",
			"Access-Control-Max-Age" => "1728000",
		]);

		return $response;
	});

	$app->options('/{module}/{action}', function (Application $app) {
		return $app->json([], 204);
	});
}

$app->get('/', function (Application $app) use ($developmentMode) {
	return $app->json(["ok" => true, "env" => $developmentMode ? "dev" : "prod"], 200);
});

$app->post('/search/area-autocomplete', function (Application $app, Request $request) {
	$phrase = $request->request->get("phrase");

	$sql = <<<END_SQL
				SELECT 
					area.id,
					area.town_name city_part,
					area.name title,
					area.county_name district_title,
					area.region_name area_title,
					CASE WHEN area.name ILIKE :phrase THEN 1
                         WHEN area.town_name ILIKE :phrase THEN 2
                    END AS priority
				FROM
					src_kn.area
				WHERE
					area.name ILIKE :phrase OR area.town_name ILIKE :phrase
				ORDER BY
					priority ASC
END_SQL;
	$data = $app["db"]->fetchAll($sql, ["phrase" => "%{$phrase}%"]);

	return $app->json($data, 200);
});

$app->post('/search/owners', function (Application $app, Request $request) {
	$filters = [];
	if ($request->request->has("name") && $request->request->has("surname")) {
		$filters["name"] = $request->request->get("name") . " " . $request->request->get("surname");
	} elseif ($request->request->has("company")) {
		$filters["name"] = $request->request->get("company");
	}
	$street = $request->request->get("street", "");
	$zip = $request->request->get("zip", "");
	$city = $request->request->get("city", "");
	if ($street !== "" && $city !== "" && $zip !== "") {
		$filters["address"] = $street . " " . $zip . " " . $city;
	} elseif ($street !== "" || $city !== "" || $zip !== "") {
		$filters["address_like"] = trim($street . " " . $zip . " " . $city);
	}
	if ($request->request->has("lv")) {
		$filters["lv"] = $request->request->get("lv");
	}
	if ($request->request->has("areas")) {
		$filters["areas"] = $request->request->get("areas");
	}

	$sql = <<<END_SQL
				SELECT 
					opsub.id,
					opsub.identification,
					opsub.name,
					opsub.address
				FROM
					src_kn.opsub2par
				LEFT JOIN
					src_kn.opsub ON opsub2par.opsub_id = opsub.id
				LEFT JOIN
					src_kn.par ON opsub2par.par_id = par.id
				WHERE

END_SQL;

	$criterias = $types = $orderBy = [];
	foreach ($filters as $key => $criteria) {
		switch ($key) {
			case "name":
				$criterias[] = "opsub.name = :name";
				break;
			case "lv":
				$criterias[] = "par.lv_num = :lv";
				break;
			case "areas":
				$criterias[] = "par.area_id IN (:areas)";
				$types["areas"] = \Doctrine\DBAL\Connection::PARAM_INT_ARRAY;
				break;
			case "address":
				$criterias[] = "opsub.address = :address";
				break;
			case "address_like":
				$criterias[] = "opsub.address % :address_like";
				$orderBy[] = "similarity(:address_like, opsub.address) DESC";
				break;
		}
	}
	if (count($criterias) === 0) {
		return $app->json(["error" => "Not enough criteria", "code" => 400], 400);
	}
	$sql .= implode(" AND ", $criterias);
	$sql .= " GROUP BY opsub.id";
	if (count($orderBy) > 0) {
		$sql .= " ORDER BY " . implode(",", $orderBy);
	}
	$data = $app["db"]->fetchAll($sql, $filters, $types);
	foreach ($data as &$item) {
		$item["id"] = intval($item["id"]);
	}

	return $app->json($data, 200);
});

$app->post('/search/areas', function (Application $app, Request $request) {
	$opsubIds = $request->request->all();

	$sql = <<<END_SQL
				SELECT 
					opsub2par.opsub_id,
					opsub.identification,
					opsub.name,
					area.name area_name,
					area.id area_id,
					area.county_name district
				FROM
					src_kn.opsub2par
				LEFT JOIN
					src_kn.par ON opsub2par.par_id = par.id
				LEFT JOIN
					src_kn.opsub ON opsub2par.opsub_id = opsub.id
				LEFT JOIN
					src_kn.area ON par.area_id = area.id
				WHERE
					opsub2par.opsub_id IN (?)
END_SQL;

	$data = $app["db"]->fetchAll($sql, [$opsubIds], [\Doctrine\DBAL\Connection::PARAM_INT_ARRAY]);

	$owners = [];
	foreach ($data as $row) {
		if (!isset($owners[$row["opsub_id"]])) {
			$owners[$row["opsub_id"]] = [
				"id" => intval($row["opsub_id"]),
				"name" => $row["name"],
				"identification" => $row["identification"],
				"areas" => [],
			];
		}
		$owners[$row["opsub_id"]]["areas"][intval($row["area_id"])] = [
			"id" => intval($row["area_id"]),
			"name" => $row["area_name"],
			"district" => $row["district"],
		];
	}
	foreach ($owners as $opsubId => $ownerData) {
		$owners[$opsubId]["areas"] = array_values($ownerData["areas"]);
	}

	return $app->json(array_values($owners), 200);
});

$app->post('/search/lands', function (Application $app, Request $request) {
	$json = $request->getContent();
	$chosenOwnersWitAreas = json_decode($json, true);

	$sql = <<<END_SQL
				SELECT 
					opsub2par.par_id,
					opsub2par.opsub_id,
					opsub.identification,
					opsub.name,
					area.name area_name,
					area.id area_id,
					area.county_name district,
					area.competency,
					drupoz.name land_type_name,
					drupoz.id drupoz_id,
					opsub2par.podil_citatel,
					opsub2par.podil_jmenovatel,
					opsub2par.land_area_ratio maintained_size,
					opsub2par.price_avg_ratio,
					CONCAT(par.kmenove_cislo_par, '/', par.poddeleni_cisla_par) as par_number,
					par.agri_land_pct,
					par.deposit_cnt,
					par.onus_cnt,
					par.land_area total_size,
					par.lv_num lv
				FROM
					src_kn.opsub2par
				LEFT JOIN
					src_kn.par ON opsub2par.par_id = par.id
				LEFT JOIN
					src_kn.opsub ON opsub2par.opsub_id = opsub.id
				LEFT JOIN
					src_kn.area ON par.area_id = area.id
				LEFT JOIN
					src_kn.drupoz ON par.drupoz_id = drupoz.id
				WHERE
					opsub2par.opsub_id IN (?) AND par.area_id IN (?)
END_SQL;

	$opsubIds = $areaIds = $expectedPars = [];
	foreach ($chosenOwnersWitAreas as $ownerAndAreas) {
		$opsubIds[$ownerAndAreas["id"]] = $ownerAndAreas["id"];
		foreach ($ownerAndAreas["areas"] as $area) {
			$areaIds[$area["id"]] = $area["id"];
			$tuple = $ownerAndAreas["id"] . "_" . $area["id"];
			$expectedPars[$tuple] = $tuple;
		}
	}

	$data = $app["db"]->fetchAll($sql, [$opsubIds, $areaIds], [\Doctrine\DBAL\Connection::PARAM_INT_ARRAY, \Doctrine\DBAL\Connection::PARAM_INT_ARRAY]);

	$owners = [];
	foreach ($data as $row) {
		if (
			!in_array($row["opsub_id"], $opsubIds)
			|| !in_array($row["area_id"], $areaIds)
			|| !in_array($row["opsub_id"] . "_" . $row["area_id"], $expectedPars)
		) {
			continue;
		}
		if (!isset($owners[$row["opsub_id"]])) {
			$owners[$row["opsub_id"]] = [
				"id" => intval($row["opsub_id"]),
				"name" => $row["name"],
				"identification" => $row["identification"],
				"landTypes" => [],
			];
		}
		if (!isset($owners[$row["opsub_id"]]["landTypes"][$row["drupoz_id"]])) {
			$owners[$row["opsub_id"]]["landTypes"][$row["drupoz_id"]] = [
				"id" => intval($row["drupoz_id"]),
				"name" => $row["land_type_name"],
				"size" => 0,
				"lands" => [],
			];
		}
		$owners[$row["opsub_id"]]["landTypes"][$row["drupoz_id"]]["size"] += $row["maintained_size"];

		$row["ownership"] = round($row["podil_citatel"]) . "/" . round($row["podil_jmenovatel"]);

		$owners[$row["opsub_id"]]["landTypes"][$row["drupoz_id"]]["lands"][] = [
			"id" => intval($row["par_id"]),
			"identification" => $row["par_number"],
			"type" => $row["land_type_name"],
			"size" => floatval($row["maintained_size"]),
			"totalSize" => floatval($row["total_size"]),
			"ownership" => $row["ownership"],
			"maintainedPercent" => floatval($row["agri_land_pct"] != "" ? $row["agri_land_pct"] : 0),
			"lv" => $row["lv"],
			"ku" => $row["area_name"],
			"status" => ($row["deposit_cnt"] > 0 ? "Z" : "") . ($row["onus_cnt"] > 0 && $row["deposit_cnt"] > 0 ? ", " : "") . ($row["onus_cnt"] > 0 ? "V" : ""),
			"competency" => floatval($row["competency"]),
			"landPrice" => floatval($row["price_avg_ratio"] / $row["maintained_size"]),
		];
	}
	foreach ($owners as $key => $ownerData) {
		$owners[$key]["landTypes"] = array_values($ownerData["landTypes"]);
	}

	return $app->json(array_values($owners), 200);
});

define('EXPORT_COLNAME_AREAS', 'čísla parcel');
define('EXPORT_COLNAME_AREA_TOTAL', "prodávaná celkem");
define('EXPORT_COLNAME_DRUPOZ_OTHER', "ostatní plocha (m2)");
define('EXPORT_COLNAME_DRUPOZ_OTHER_PERCENT', "ostatní plocha (%)");
define('EXPORT_COLNAME_LV_COUNT', "#LV");
define('EXPORT_COLNAME_AREA_COUNT', "#PARCEL");
define('EXPORT_COLNAME_AREA_LV_COUNT', "#PARCEL + #LV");
define('EXPORT_COLNAME_COMPETENCY', "KONKURENCE ZEM. SUBJ. %");
define('EXPORT_COLNAME_LAND_PRICE_TOTAL', "ÚŘED CENA VÝMĚRY");
define('EXPORT_COLNAME_LAND_PRICE', "ÚŘED CENA 1m2");
define('EXPORT_COLNAME_MAINTAINED_PERCENT', "% OBHOSPOD.");
define('EXPORT_COLNAME_SELLING_TOTAL', 'PRODÁVANÁ CELKEM');

$app->post("/search/export-to-xml", function (Application $app, Request $request) {
	$json = $request->getContent();
	$dealData = json_decode($json, true);

	$sqlParcelData = <<<END_SQL
				SELECT 
					opsub.name as owner_name,
					opsub.identification as owner_identification,
					opsub.address as owner_address,
					CONCAT(par.kmenove_cislo_par, '/', par.poddeleni_cisla_par) as number,
					area.name area_name,
					area.region_name area_region,
					drupoz.name drupoz_name
				FROM
					src_kn.opsub2par
				LEFT JOIN
					src_kn.par ON opsub2par.par_id = par.id
				LEFT JOIN
					src_kn.opsub ON opsub2par.opsub_id = opsub.id
				LEFT JOIN
					src_kn.area ON par.area_id = area.id
				LEFT JOIN
					src_kn.drupoz ON par.drupoz_id = drupoz.id
				WHERE
					opsub2par.par_id = :parId AND opsub2par.opsub_id = :opsubId
END_SQL;

	// remove old exports
	$directory = PROJECT_ROOT . '/var/tmp';
	$scannedDirectory = array_diff(scandir($directory), array('..', '.', '.gitkeep'));
	foreach ($scannedDirectory as $file) {
		unlink($directory . "/" . $file);
	}

	$newFilePath = PROJECT_ROOT . '/var/tmp/' . uniqid("export") . ".xlsx";

	/** @var Box\Spout\Writer\XLSX\Writer $writer */
	$writer = WriterFactory::create(Type::XLSX);
	$writer->openToFile($newFilePath);

	$boldStyle = (new StyleBuilder())
		->setFontBold()
		->build();

	$columns = [
		"ID dealu" => "",
		"Vlastník" => "",
		"r.č. (věk)" => "",
		"Bydliště" => "",
		"Název k.ú." => "",
		"název Obce" => "",
		"Název Kraje" => "",
		"čísla LV" => "",
		EXPORT_COLNAME_AREAS => "",
		"podíl" => "",
		"Prodávaná výměra LV" => "",
		EXPORT_COLNAME_AREA_TOTAL => "",
		"orná půda (m2)" => "",
		"trvalý travní porost (m2)" => "",
		EXPORT_COLNAME_DRUPOZ_OTHER => "",
		"zahrada (m2)" => "",
		"ovocný sad (m2)" => "",
		"lesní pozemek (m2)" => "",
		"vodní plocha (m2)" => "",
		"zastavěná plocha a nádvoří (m2)" => "",
		"vinice (m2)" => "",
		"chmelnice (m2)" => "",
		EXPORT_COLNAME_SELLING_TOTAL => "",
		"orná půda (%)" => 0,
		"trvalý travní porost (%)" => 0,
		EXPORT_COLNAME_DRUPOZ_OTHER_PERCENT => 0,
		"zahrada (%)" => 0,
		"ovocný sad (%)" => 0,
		"lesní pozemek (%)" => 0,
		"vodní plocha (%)" => 0,
		"zastavěná plocha a nádvoří (%)" => 0,
		"vinice (%)" => 0,
		"chmelnice (%)" => 0,
		EXPORT_COLNAME_MAINTAINED_PERCENT => "",
		EXPORT_COLNAME_LAND_PRICE => "",
		EXPORT_COLNAME_LAND_PRICE_TOTAL => "",
		EXPORT_COLNAME_COMPETENCY => "",
		"POČET ZEM. SUBJ." => "",
		EXPORT_COLNAME_AREA_COUNT => "",
		EXPORT_COLNAME_LV_COUNT => "",
		EXPORT_COLNAME_AREA_LV_COUNT => "",
	];
	$writer->addRowWithStyle(array_keys($columns), $boldStyle);

	$dealId = date("YmdHi");
	$parcelGroups = $parIds = $lvNumbers = [];
	$firstDataRow = $columns;
	foreach ($dealData["selectedLands"] as $ownerData) {
		foreach ($ownerData["lands"] as $ownerArea) {
			$parcelGroupKey = $ownerData["id"] . "_" . $ownerArea["ku"] . "_" . $ownerArea["lv"];

			$parcelData = $app["db"]->fetchAssoc($sqlParcelData, ["parId" => $ownerArea["id"], "opsubId" => $ownerData["id"]]);
			if (isset($parcelGroups[$parcelGroupKey])) {
				$parcelGroups[$parcelGroupKey][EXPORT_COLNAME_AREAS][] = $parcelData["number"];
				$parcelGroups[$parcelGroupKey][EXPORT_COLNAME_AREA_TOTAL] += intval($ownerArea["size"]);
			} else {
				$parcelGroups[$parcelGroupKey] = [
					"ID dealu" => $dealId,
					"Vlastník" => $parcelData["owner_name"],
					"r.č. (věk)" => $parcelData["owner_identification"],
					"Bydliště" => $parcelData["owner_address"],
					"Název k.ú." => $ownerArea["ku"],
					"název Obce" => $parcelData["area_name"],
					"Název Kraje" => $parcelData["area_region"],
					"čísla LV" => $ownerArea["lv"],
					EXPORT_COLNAME_AREAS => [
						$parcelData["number"],
					],
					"podíl" => $ownerArea["maintainedPercent"],
					"Prodávaná výměra LV" => $ownerArea["size"],
					EXPORT_COLNAME_AREA_TOTAL => $ownerArea["size"],
					"orná půda (m2)" => 0,
					"trvalý travní porost (m2)" => 0,
					EXPORT_COLNAME_DRUPOZ_OTHER => 0,
					"zahrada (m2)" => 0,
					"ovocný sad (m2)" => 0,
					"lesní pozemek (m2)" => 0,
					"vodní plocha (m2)" => 0,
					"zastavěná plocha a nádvoří (m2)" => 0,
					"vinice (m2)" => 0,
					"chmelnice (m2)" => 0,
				];
				$lvNumbers[$ownerArea["lv"]] = 1;
			}
			if (isset($parcelGroups[$parcelGroupKey][$parcelData["drupoz_name"] . " (m2)"])) {
				$parcelGroups[$parcelGroupKey][$parcelData["drupoz_name"] . " (m2)"] += intval($ownerArea["size"]);
			} else {
				$parcelGroups[$parcelGroupKey][EXPORT_COLNAME_DRUPOZ_OTHER] += intval($ownerArea["size"]);
			}

			$firstDataRow[EXPORT_COLNAME_SELLING_TOTAL] += intval($ownerArea["size"]);
			if (isset($firstDataRow[$parcelData["drupoz_name"] . " (%)"])) {
				$firstDataRow[$parcelData["drupoz_name"] . " (%)"] = intval($ownerArea["size"]);
			} else {
				$firstDataRow[EXPORT_COLNAME_DRUPOZ_OTHER_PERCENT] = intval($ownerArea["size"]);
			}
			$firstDataRow[EXPORT_COLNAME_MAINTAINED_PERCENT] += floatval($ownerArea["maintainedPercent"]);
			$firstDataRow[EXPORT_COLNAME_LAND_PRICE] += floatval($ownerArea["landPrice"]);
			$firstDataRow[EXPORT_COLNAME_LAND_PRICE_TOTAL] += intval($ownerArea["landPrice"] * $ownerArea["size"]);
			$firstDataRow[EXPORT_COLNAME_COMPETENCY] += floatval($ownerArea["competency"]);
			$parIds[$ownerArea["id"]] = 1;
		}
	}

	$firstDataRow["orná půda (%)"] = ($firstDataRow["orná půda (%)"] * 100) / $firstDataRow[EXPORT_COLNAME_SELLING_TOTAL];
	$firstDataRow["trvalý travní porost (%)"] = ($firstDataRow["trvalý travní porost (%)"] * 100) / $firstDataRow[EXPORT_COLNAME_SELLING_TOTAL];
	$firstDataRow[EXPORT_COLNAME_DRUPOZ_OTHER_PERCENT] = ($firstDataRow[EXPORT_COLNAME_DRUPOZ_OTHER_PERCENT] * 100) / $firstDataRow[EXPORT_COLNAME_SELLING_TOTAL];
	$firstDataRow["zahrada (%)"] = ($firstDataRow["zahrada (%)"] * 100) / $firstDataRow[EXPORT_COLNAME_SELLING_TOTAL];
	$firstDataRow["ovocný sad (%)"] = ($firstDataRow["ovocný sad (%)"] * 100) / $firstDataRow[EXPORT_COLNAME_SELLING_TOTAL];
	$firstDataRow["lesní pozemek (%)"] = ($firstDataRow["lesní pozemek (%)"] * 100) / $firstDataRow[EXPORT_COLNAME_SELLING_TOTAL];
	$firstDataRow["vodní plocha (%)"] = ($firstDataRow["vodní plocha (%)"] * 100) / $firstDataRow[EXPORT_COLNAME_SELLING_TOTAL];
	$firstDataRow["zastavěná plocha a nádvoří (%)"] = ($firstDataRow["zastavěná plocha a nádvoří (%)"] * 100) / $firstDataRow[EXPORT_COLNAME_SELLING_TOTAL];
	$firstDataRow["vinice (%)"] = ($firstDataRow["vinice (%)"] * 100) / $firstDataRow[EXPORT_COLNAME_SELLING_TOTAL];
	$firstDataRow["chmelnice (%)"] = ($firstDataRow["chmelnice (%)"] * 100) / $firstDataRow[EXPORT_COLNAME_SELLING_TOTAL];

	$firstDataRow[EXPORT_COLNAME_AREA_COUNT] = count($parIds);
	$firstDataRow[EXPORT_COLNAME_LV_COUNT] = count($lvNumbers);

	$firstDataRow[EXPORT_COLNAME_MAINTAINED_PERCENT] = round($firstDataRow[EXPORT_COLNAME_MAINTAINED_PERCENT] / $firstDataRow[EXPORT_COLNAME_AREA_COUNT]);
	$firstDataRow[EXPORT_COLNAME_LAND_PRICE] = round($firstDataRow[EXPORT_COLNAME_LAND_PRICE] / $firstDataRow[EXPORT_COLNAME_AREA_COUNT]);
	$firstDataRow[EXPORT_COLNAME_COMPETENCY] = round($firstDataRow[EXPORT_COLNAME_COMPETENCY] / $firstDataRow[EXPORT_COLNAME_AREA_COUNT]);

	$firstDataRow[EXPORT_COLNAME_AREA_LV_COUNT] = $firstDataRow[EXPORT_COLNAME_LV_COUNT] + $firstDataRow[EXPORT_COLNAME_AREA_COUNT];

	$first = true;
	foreach ($parcelGroups as $row) {
		if ($first) {
			$row = array_merge($firstDataRow, $row);
			$first = false;
		}
		$row[EXPORT_COLNAME_AREAS] = implode(", ", $row[EXPORT_COLNAME_AREAS]);

		$writer->addRow($row);
	}

	$writer->close();

	return $app->sendFile(
		$newFilePath,
		200,
		[
			"Access-Control-Expose-Headers" => "Content-Disposition",
		],
		"attachment"
	);
});

$app->run();