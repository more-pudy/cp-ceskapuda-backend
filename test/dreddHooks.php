<?php
use Dredd\Hooks;

Hooks::beforeValidation("Výběr půdy varianta s vlastníky > Odeslání výběru půdy", function(&$transaction) {
	$headersAsArray = json_decode(json_encode($transaction->real->headers), true);
	if (isset($headersAsArray["content-disposition"]) && $headersAsArray["content-type"] == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
		// method returns a file via Content-Disposition
		$transaction->skip = true;
	} else {
		$transaction->fail = true;
	}
});